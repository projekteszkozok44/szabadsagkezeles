# SzabadságKezelő

## A webalkalmazásba bármilyen cég regisztrálhat főnököket, illetve alkalmazottakat.

* Az alkalmazottak kiválaszthatnak és igényelhetnek szabadság-időpontokat,  
* választhatnak fizetett, vagy nem fizetett szabadságot, 
* ezen felül látják, hogy egy-egy nem fizetett szabadság esetén hány órával kell 
 többet dolgozniuk majd az adott hónapban.

* A kérések a főnöknek regisztrált felhasználóknál jelennek meg, 
 amelyeket vagy jóváhagy, vagy elutasít.

* A főnökök regisztrálhatnak új dolgozókat, illetve ők módosíthatják az adott 
 munkavállaló adatait.

* A szabadság igénylésekhez megjegyzések fűzhetőek. 
* A letiltott napok közül nem 
 lehet választani, mert akkor nem lenne elég ember a cégnél.

### A rendszer a kérések elbírálásáról emailben tájékoztatja a dolgozókat.

# A .jar futtatása után a webalkalmazás a localhost:8080-n érhető el.

## A kezdőoldalon lehetőség van a regisztrációra, vagy bejelentkezésre
![Kezdőoldal](FreeTime/images/home_reg.png "Kezdőoldal")

## A regisztrációs felületen létrehozhatunk dolgozó, vagy főnök, vagy admin fiókot
![Regisztráció](FreeTime/images/reg.png "Regisztráció")

## A szabadság igénylő felületen dolgozóként kiválaszthatjuk a kezdő-, illetve végnapot, és hogy fizetett szabadságot kérünk-e, valamint megjegyzést hagyhatunk
![Igénylés](FreeTime/images/request.png "Igénylés")

## Főnök szerepben a jóváhagyás oldalon jelennek meg a szabadság igénylések, amiket el lehet fogadni, vagy el lehet utasítani
![Jóváhagyás](FreeTime/images/approve.png "Jóváhagyás")

## Főnök szerepben lehetőség van a munkavállalók adatainak módosítására
![Módosítás](FreeTime/images/modify.png "Módosítás")

## Főnök szerepben lehetőség van új munkavállalók felvételére
![Felvétel](FreeTime/images/addworker.png "Felvétel")

## Főnök szerepben az infó oldalon listázhatjuk a dolgozókat, látjuk ki van éppen szabadságon
![Info](FreeTime/images/info.png "Info")
