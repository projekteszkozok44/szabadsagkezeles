package hu.elte.projekteszkozok.FreeTime.repository;

import hu.elte.projekteszkozok.FreeTime.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import hu.elte.projekteszkozok.FreeTime.enums.UserType;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *<H1>Az UserRepository interfesz megvalositasa</H1>
 *
 * @since 2020.04.25.
 * @author Bettina
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    List<User> findByType(UserType type);

}

