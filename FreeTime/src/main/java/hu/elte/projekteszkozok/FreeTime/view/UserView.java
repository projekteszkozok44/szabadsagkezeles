package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.enums.UserType;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.DAYS;

@Scope("vaadin-ui")
@SpringView(name = UserView.VIEW_NAME)
public class UserView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "user";

    private final UserService userService;
    private Grid<User> users;
    private User user;
    private final CheckBox filterBox = new CheckBox();

    public UserView(UserService userService) {
        this.userService = userService;
        initUser();
        if(user.getType().equals(UserType.ADMIN) || user.getType().equals(UserType.EMPLOYER)){
            initInfo();
            initGrid();
            initFilter();
        }
        if(user.getType().equals(UserType.EMPLOYEE)){
            initInfo();
        }
    }

    private void initGrid() {
        users = new Grid<>();
        users.setHeightMode(HeightMode.UNDEFINED);
        users.setWidth(100, Unit.PERCENTAGE);
        users.setBodyRowHeight(100);
        users.addColumn(User::getFirstName).setCaption("Vezetéknév");
        users.addColumn(User::getLastName).setCaption("Keresztnév").setId("lastname");
        users.addComponentColumn(user -> {
            CheckBox onHoliday = new CheckBox();
            onHoliday.setReadOnly(true);
            if(user.isOnHoliday()){
                onHoliday.setValue(true);
                return onHoliday;
            } else {
                onHoliday.setValue(false);
                return onHoliday;
            }
        }).setCaption("Szabadságon").setId("free");
        users.setStyleGenerator(item -> "v-align-center");
        addComponent(users);
    }

    private void initInfo(){
        Label name = new Label("Név: " + user.getFirstName() + " " + user.getLastName());
        String state;
        if(user.isOnHoliday())
            state = "szabadságon";
        else
            state = "dolgozik";
        Label freeTime = new Label("Szabadság státusz: " + state);
        long daysOnHoliday = 0;
        long numOfHours = 0;
        if(user.isOnHoliday() && user.getHoliday() != null) {
            if (!user.getHoliday().isPaid()) {
                daysOnHoliday = DAYS.between(user.getHoliday().getStart(), user.getHoliday().getEnd());
                numOfHours = daysOnHoliday * Long.parseLong(user.getDailyHours());
            }
        }
        Label haveToWork = new Label("Mínusz órák száma: " + numOfHours);
        addComponents(name, freeTime, haveToWork);
    }

    private void initFilter(){
        HeaderRow filterHeader = users.appendHeaderRow();
        filterBox.addValueChangeListener(e -> searchForFilters());
        filterBox.setSizeUndefined();
        filterHeader.getCell("free").setComponent(filterBox);
        Button clearButton = new Button("Szűrés törlése");
        clearButton.addClickListener(e -> filterBox.clear());
        filterHeader.getCell("lastname").setComponent(clearButton);
    }

    private void searchForFilters(){
        List<User> filtered = userService.findAll().stream().filter(i -> i.isOnHoliday() == filterBox.getValue()).collect(Collectors.toList());
        if(filtered.size() > 0){
            users.setItems(filtered);
        } else{
            users.setItems(Stream.empty());
            Notification.show("Nincs ilyen dolgozó.", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void updateGrid(){
        users.getDataProvider().refreshAll();
        users.setItems(userService.findAll());
    }

    private void initUser(){
        user = ((AdminUI) UI.getCurrent()).getUser();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        initUser();
        if(user.getType().equals(UserType.ADMIN) || user.getType().equals(UserType.EMPLOYER)) {
            updateGrid();
        }
        getUI().getPage().setTitle("FreeTime - Adatok");

    }
}
