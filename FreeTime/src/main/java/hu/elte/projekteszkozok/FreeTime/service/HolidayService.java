package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.Holiday;
import hu.elte.projekteszkozok.FreeTime.entity.User;

import java.util.List;

public interface HolidayService {

    Holiday add(Holiday holiday);

    void addHoliday(User user, Holiday holiday);

    List<Holiday> listAll();


    void deleteHoliday(Holiday toDel);

    void deleteAllHolidays();


}
