package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.Editor;
import com.vaadin.ui.components.grid.EditorSaveListener;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Scope("vaadin-ui")
@SpringView(name = ModifyView.VIEW_NAME)
public class ModifyView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "modify";

    private UserService userService;

    private Grid<User> users;
    private final TextField nameFilter = new TextField();
    private TextField firstNameEditor;
    private TextField lastNameEditor;
    private TextField monthlyEditor;
    private TextField dailyEditor;
    private Editor editor;

    public ModifyView(UserService userService){
        this.userService = userService;

        setHeightUndefined();
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
        initGrid();
        initFilter();
    }

    private void initGrid(){
        firstNameEditor = new TextField();
        lastNameEditor = new TextField();
        monthlyEditor = new TextField();
        dailyEditor = new TextField();
        users = new Grid<>();
        users.setHeightMode(HeightMode.UNDEFINED);
        users.setWidth(100, Unit.PERCENTAGE);
        editor = users.getEditor().setEnabled(true);
        editor.addSaveListener((EditorSaveListener<User>) event -> {
            User user = event.getBean();
            userService.updateUser(user);
            Notification.show("Adat módosítva!", Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_RIGHT);
        });
        users.setHeightMode(HeightMode.UNDEFINED);
        users.setSizeFull();
        users.setSelectionMode(Grid.SelectionMode.NONE);
        users.addColumn(User::getFirstName).setEditorComponent(new TextField(), User::setFirstName).setCaption("Vezetéknév").setId("firstName");
        users.addColumn(User::getLastName).setCaption("Keresztnév").setEditorComponent(lastNameEditor, User::setLastName).setId("lastName");
        users.addColumn(User::getMonthlyHours).setCaption("Havi óra").setEditorComponent(monthlyEditor, User::setMonthlyHours);
        users.addColumn(User::getDailyHours).setCaption("Napi óra").setEditorComponent(dailyEditor, User::setDailyHours);
        users.addComponentColumn(this::buildDelButton).setCaption("Törlés");
        users.setStyleGenerator(item -> "v-align-center");
        addComponent(users);
    }

    private Button buildDelButton(User user){
        Button button = new Button(VaadinIcons.DEL);
        button.addStyleName(ValoTheme.BUTTON_DANGER);
        button.addClickListener(e -> {
            userService.deleteUser(user);
            updateGrid();
        });
        return button;
    }

    private void initFilter(){
        HeaderRow filterHeader = users.appendHeaderRow();
        nameFilter.setPlaceholder("Szűrés vezetéknévre...");
        nameFilter.addValueChangeListener(e -> searchForFilters());
        nameFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        nameFilter.setSizeUndefined();
        nameFilter.setValueChangeMode(ValueChangeMode.LAZY);
        Button clearFilterButton = new Button("Szűrés törlése");
        clearFilterButton.setStyleName(ValoTheme.BUTTON_TINY);
        clearFilterButton.addClickListener(e -> clearFilters());
        filterHeader.getCell("firstName").setComponent(nameFilter);
        filterHeader.getCell("lastName").setComponent(clearFilterButton);
    }

    private void searchForFilters(){
        List<User> filtered = userService.findAll().stream().filter(x -> x.getFirstName().toLowerCase().contains(nameFilter.getValue().toLowerCase())).collect(Collectors.toList());
        if(filtered.size() > 0){
            users.setItems(filtered);
        } else{
            users.setItems(Stream.empty());
            Notification.show("Nincs ilyen vezetéknév.", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void clearFilters(){
        nameFilter.clear();
    }

    private void updateGrid(){
        users.getDataProvider().refreshAll();
        users.setItems(userService.findAll());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("FreeTime - Módosítás");
        clearFilters();
        updateGrid();
        if(users.getDataProvider().size(new Query<>()) == 0)
            Notification.show("Nem található egy alkalmazott sem.", Notification.Type.ERROR_MESSAGE);
    }


}
