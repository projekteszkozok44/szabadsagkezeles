package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.data.Validator;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.data.Binder;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.enums.UserType;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.Objects;


@Scope("vaadin-ui")
@SpringView(name = RegisterView.VIEW_NAME)
public class RegisterView extends FormLayout implements View {

    public static final String VIEW_NAME = "register";

    private UserService userService;
//    private EmailService emailService;

    private Binder<User> binder;
    private FormLayout registerForm;

    public RegisterView(UserService userService){
        this.userService = userService;
//        this.emailService = emailService;

        setMargin(true);
        initHeader();
        setupBinder();
    }

    private void initHeader(){
        Label register = new Label("Új felhasználó regisztálása");
        register.setStyleName(ValoTheme.LABEL_H3);
        addComponent(register);
    }

    private void setupBinder(){
        binder = new Binder<>();

        TextField firstNameField = new TextField("Vezetéknév:");
        firstNameField.focus();
        binder.forField(firstNameField).asRequired("Kötelező!").withValidator(name -> name.length() >= 3,
                "Minimum 3 karakter!").bind(User::getFirstName, User::setFirstName);
        firstNameField.setIcon(VaadinIcons.TEXT_LABEL);
        firstNameField.setWidth(250, Unit.PIXELS);
        firstNameField.setPlaceholder("Vezetéknév");
        TextField lastNameField = new TextField("Keresztnév:");
        binder.forField(lastNameField).asRequired("Kötelező!").withValidator(name -> name.length() >= 3,
                "Minimum 3 karakter!").bind(User::getLastName, User::setLastName);
        lastNameField.setIcon(VaadinIcons.TEXT_LABEL);
        lastNameField.setWidth(250, Unit.PIXELS);
        lastNameField.setPlaceholder("Keresztnév");

        TextField emailField = new TextField("Email:");
        binder.forField(emailField).asRequired("Kötelező!").withValidator(new EmailValidator("Érvénytelen e-mail cím!")).bind(User::getEmail, User::setEmail);
        emailField.setIcon(VaadinIcons.AT);
        emailField.setWidth(250, Unit.PIXELS);
        emailField.setPlaceholder("valami@valami.hu");

        TextField monthlyHours = new TextField("Havi óraszám:");
        binder.forField(monthlyHours).asRequired("Kötelező!").bind(User::getMonthlyHours, User::setMonthlyHours);
        monthlyHours.setIcon(VaadinIcons.CLOCK);
        monthlyHours.setWidth(250, Unit.PIXELS);
        monthlyHours.setPlaceholder("180");

        TextField dailyHours = new TextField("Napi óraszám:");
        binder.forField(dailyHours).asRequired("Kötelező!").bind(User::getDailyHours, User::setDailyHours);
        dailyHours.setIcon(VaadinIcons.CLOCK);
        dailyHours.setWidth(250, Unit.PIXELS);
        dailyHours.setPlaceholder("6");

        RadioButtonGroup<UserType> typeField = new RadioButtonGroup<>("Típus:");
//        typeField.setItems(UserType.EMPLOYEE, UserType.EMPLOYER, UserType.ADMIN);
        typeField.setItems(UserType.values());
        binder.forField(typeField).asRequired("Kötelező!").bind(User::getType, User::setType);
        typeField.setIcon(VaadinIcons.MALE);

        TextField phoneField = new TextField("Telefonszám:");
        binder.forField(phoneField).withValidator(phone -> phone.length() == 12 || phone.isEmpty(),
                "Formátum: +36xxxxxxxxx").asRequired("Kötelező telefonszámot megadni!").bind(User::getPhone, User::setPhone);
        phoneField.setIcon(VaadinIcons.PHONE);
        phoneField.setWidth(150, Unit.PIXELS);
        phoneField.setPlaceholder("+36xxxxxxxxx");


        PasswordField passwordField = new PasswordField("Jelszó:");
        binder.forField(passwordField).asRequired("Kötelező!").withValidator(pass -> pass.length() >= 6,
                "A jelszó minimum 6 karakter!" ).bind(User::getPassword, User::setPassword);
        passwordField.setIcon(VaadinIcons.PASSWORD);
        passwordField.setWidth(150, Unit.PIXELS);

        PasswordField checkPasswordField = new PasswordField("Jelszó mégegyszer:");
        binder.forField(checkPasswordField).asRequired("Kötelező a megerősítés.").bind(User::getPassword, (person, password) -> {});
        checkPasswordField.setIcon(VaadinIcons.PASSWORD);
        checkPasswordField.setWidth(150, Unit.PIXELS);

        binder.withValidator(Validator.from(user -> {
            if(passwordField.isEmpty() || checkPasswordField.isEmpty()){
                return true;
            } else {
                return Objects.equals(passwordField.getValue(), checkPasswordField.getValue());
            }
        }, "A két jelszónak meg kell egyeznie!"));

        Label validationStatus = new Label();
        binder.setStatusLabel(validationStatus);

        Button registerButton = new Button("Regisztráció");
        registerButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        registerButton.setEnabled(false);
        registerButton.addClickListener(event -> {
            try{
                userService.add(binder.getBean());
                Notification.show("Sikeres regisztráció!", "Jelentkezz be!",
                        Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_LEFT);
                getUI().getNavigator().navigateTo("/login");
            }catch(IllegalArgumentException e){
                Notification.show("Ez az email cím foglalt!", Notification.Type.ERROR_MESSAGE);
            }
        });
        binder.addStatusChangeListener(event -> registerButton.setEnabled(binder.isValid()));

        Button loginButton = new Button("Bejelentkezés");
        loginButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        loginButton.addClickListener(event -> getUI().getPage().setLocation("/login"));
        HorizontalLayout buttons = new HorizontalLayout(loginButton, registerButton);
        addComponents(firstNameField, lastNameField, monthlyHours, dailyHours, emailField, passwordField, checkPasswordField,
                phoneField, typeField, validationStatus, buttons);
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("FreeTime - Regisztráció");
        binder.setBean(new User());
    }

}
