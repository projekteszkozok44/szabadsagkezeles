package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.User;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.slf4j.LoggerFactory.getLogger;

public class EmailServiceImpl implements EmailService {

    @Value("birdsaverapp@gmail.com")
    private String MESSAGE_FROM;

    private final JavaMailSender javaMailSender;
    private static final Logger LOGGER = getLogger(EmailService.class);

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
        LOGGER.info("EmailService built.");
    }

    @Override
    public void sendRegistrationMessage(User user) {
        SimpleMailMessage registerMessage;
        try{
            registerMessage = new SimpleMailMessage();
            registerMessage.setFrom(MESSAGE_FROM);
            registerMessage.setTo(user.getEmail());
            registerMessage.setSubject("FreeTime - Sikeres regisztráció");
            registerMessage.setText("Kedves " + user.getFirstName() + " " + user.getLastName() + "!\n\nSikeresen regisztráltál a FreeTime oldalra!" +
                    "\nReméljük, hogy rengeteg szabadságot kapsz az évben." +
                    "\n\nÜdv," +
                    "\nFreeTime");
            javaMailSender.send(registerMessage);
            LOGGER.info("Email was sent about registration to: " + user.getEmail());
        } catch (MailException e){
            LOGGER.error("An error occured while sending register message: " + user.getEmail());
        } catch (IllegalArgumentException e){
            LOGGER.error("User with this email " + user.getEmail() + " is not found.");
        }
    }

    @Override
    public void sendApprovedMessage(User user) {
        SimpleMailMessage approvedMessage;
        try{
            approvedMessage = new SimpleMailMessage();
            approvedMessage.setFrom(MESSAGE_FROM);
            approvedMessage.setTo(user.getEmail());
            approvedMessage.setSubject("FreeTime - Igény jóváhagyva");
            approvedMessage.setText("Kedves " + user.getFirstName() + " " + user.getLastName() + "!\n\nA szabaságigénylésed sikeres volt." +
                    "\nA szabadság intervalluma: " +
                    user.getHoliday().getStart() + "-tól " + user.getHoliday().getEnd() + "-ig." +
                    "\nJó pihenést kívánunk!" +
                    "\n\nÜdv," +
                    "\nFreeTime");
            javaMailSender.send(approvedMessage);
            LOGGER.info("Email was sent about approve to: " + user.getEmail());
        } catch (MailException e){
            LOGGER.error("An error occured while sending approve message: " + user.getEmail());
        } catch (IllegalArgumentException e){
            LOGGER.error("User with this email " + user.getEmail() + " is not found.");
        }
    }

    @Override
    public void sendDeclinedMessage(User user) {
        SimpleMailMessage declinedMessage;
        try{
            declinedMessage = new SimpleMailMessage();
            declinedMessage.setFrom(MESSAGE_FROM);
            declinedMessage.setTo(user.getEmail());
            declinedMessage.setSubject("FreeTime - Igény elutasítva");
            declinedMessage.setText("Kedves " + user.getFirstName() + " " + user.getLastName() + "!\n\nA szabaságigénylésed sikertelen volt." +
                    "\nKénytelen leszel bemenni dolgozni, próbálkozz később!" +
                    "\n\nÜdv," +
                    "\nFreeTime");
            javaMailSender.send(declinedMessage);
            LOGGER.info("Email was sent about decline to: " + user.getEmail());
        } catch (MailException e){
            LOGGER.error("An error occured while sending decline message: " + user.getEmail());
        } catch (IllegalArgumentException e){
            LOGGER.error("User with this email " + user.getEmail() + " is not found.");
        }
    }
}
