package hu.elte.projekteszkozok.FreeTime.enums;

/**
 *<H1>A felhasznalok tipusainak felsorolasa</H1>
 *
 * Lehet munkavallalo, munkaltato és admin.
 * @since 2020.04.25.
 * @author Bettina
 */

public enum UserType {
    EMPLOYEE,
    EMPLOYER,
    ADMIN
}
