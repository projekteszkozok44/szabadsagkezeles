package hu.elte.projekteszkozok.FreeTime.config;

import hu.elte.projekteszkozok.FreeTime.repository.HolidayRepository;
import hu.elte.projekteszkozok.FreeTime.repository.UserRepository;
import hu.elte.projekteszkozok.FreeTime.security.MyUserDetailsService;
import hu.elte.projekteszkozok.FreeTime.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *<H1>FreeTimeConfiguration osztaly megvalositasa</H1>
 *
 * @since 2020.04.25.
 * @author Bettina
 */

@Configuration
public class FreeTimeConfiguration {

    @Bean
    public EmailService emailService(JavaMailSender javaMailSender){
        return new EmailServiceImpl(javaMailSender);
    }
	
	/**
     * Felhasznaloi szolgaltatas letrehozasa.
     *
     * @param userRepository egy adott felhasznaloi gyujtemeny
     * @param passwordEncoder egy adott jelszo kodolo
     * @return letrehoz egy uj felhasznaloi szolgaltatast
     */
    @Bean
    public UserService userService(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailService emailService){
        return new UserServiceImpl(userRepository, passwordEncoder, emailService);
    }

    @Bean
    public HolidayService holidayService(HolidayRepository holidayRepository, UserRepository userRepository){
        return new HolidayServiceImpl(holidayRepository, userRepository);
    }
	
	/**
     * Jelszo kodolo
     *
     * @return visszaadja a jelszot kodolva
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService(UserRepository userRepository, UserService userService){
        return new MyUserDetailsService(userRepository, userService);
    }
}
