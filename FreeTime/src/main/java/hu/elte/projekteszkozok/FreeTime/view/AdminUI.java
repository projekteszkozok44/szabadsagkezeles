package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.*;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.enums.UserType;
import hu.elte.projekteszkozok.FreeTime.security.MyUserDetails;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringUI
@PushStateNavigation
public class AdminUI extends UI {

    @Autowired
    private UserService userService;

    public User user;
    private SpringNavigator navigator;
    private Panel viewContainer;
    private GridLayout baseLayout;

    private MenuBar menuBar;
    private MenuBar.MenuItem home;
    private MenuBar.MenuItem requestFreeTime;
    private MenuBar.MenuItem approveFreeTime;
    private MenuBar.MenuItem addWorker;
    private MenuBar.MenuItem modifyWorker;
    private MenuBar.MenuItem account;
    private MenuBar.MenuItem login;
    private MenuBar.MenuItem register;
    private MenuBar.MenuItem logout;
    private MenuBar.MenuItem info;
    private MenuBar.MenuItem admin;

    public AdminUI(SpringNavigator navigator){
        this.navigator = navigator;
        UI.setCurrent(this);
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if(!(authentication instanceof AnonymousAuthenticationToken)){
                MyUserDetails principal = (MyUserDetails) authentication.getPrincipal();
                user = principal.getUser();
            }
        } catch (NullPointerException e){
            Notification.show("Jelentkezz be!", Notification.Type.ERROR_MESSAGE);
        }

        setUpGridLayout();
        restrictVisibility();

        navigator.init(this, viewContainer);
        setNavigator(navigator);
        navigator.addView("", HomeView.class);
        navigator.setErrorView(HomeView.class);
    }

    private void setUpGridLayout(){
        this.baseLayout = new GridLayout(2, 2);
        baseLayout.setSizeFull();
        baseLayout.setMargin(true);
        baseLayout.setSpacing(true);
        baseLayout.setRowExpandRatio(0, 10);
        baseLayout.setRowExpandRatio(1, 90);
        baseLayout.setDefaultComponentAlignment(Alignment.TOP_CENTER);
        addMenuBar();
        addContentLayout();
        setContent(baseLayout);
    }

    private void addMenuBar(){
        menuBar = new MenuBar();
        home = menuBar.addItem("Főoldal", VaadinIcons.HOME_O, menuItem -> getNavigator().navigateTo(""));
        requestFreeTime = menuBar.addItem("Igénylés", VaadinIcons.USER_CLOCK, menuItem -> getNavigator().navigateTo(RequestView.VIEW_NAME));// VaadinIcons.COMMENT_O, menuItem -> getNavigator().navigateTo(RequestView.VIEW_NAME));
        approveFreeTime = menuBar.addItem("Jóváhagyás", VaadinIcons.USER_CHECK, menuItem -> getNavigator().navigateTo(ApproveView.VIEW_NAME));
        addWorker = menuBar.addItem("Felvétel", VaadinIcons.USER_STAR, menuItem -> getNavigator().navigateTo(RegisterView.VIEW_NAME));
        modifyWorker = menuBar.addItem("Módosítás", VaadinIcons.WRENCH, menuItem -> getNavigator().navigateTo(ModifyView.VIEW_NAME));
        account = menuBar.addItem("Fiók", VaadinIcons.USER, null);
        MenuBar.Command toLogin = (MenuBar.Command) menuItem -> {
            getUI().getSession().close();
            getUI().getPage().setLocation("/login");
        };
        register = account.addItem("Regisztráció", VaadinIcons.KEY_O, menuItem -> getNavigator().navigateTo(RegisterView.VIEW_NAME));
        MenuBar.Command toLogout = (MenuBar.Command) menuItem -> {
            getUI().getSession().close();
            getUI().getPage().setLocation("/logout");
        };
        login = account.addItem("Bejelentkezés", VaadinIcons.SIGN_IN, toLogin);
        logout = account.addItem("Kijelentkezés", VaadinIcons.SIGN_OUT, toLogout);
        if(getUser() != null){
            info = menuBar.addItem("Hello " + getUser().getLastName() + "!", VaadinIcons.INFO_CIRCLE, menuItem -> getNavigator().navigateTo(UserView.VIEW_NAME));
        }
        admin = menuBar.addItem("Admin", VaadinIcons.FIRE, menuItem -> getNavigator().navigateTo(AdminView.VIEW_NAME));
        menuBar.setSizeUndefined();
        baseLayout.addComponent(menuBar, 0, 0, 1, 0);
        baseLayout.setComponentAlignment(menuBar, Alignment.TOP_CENTER);
    }

    private void addContentLayout(){
        viewContainer = new Panel();
        viewContainer.setHeight("100%");
        viewContainer.setWidth("100%");
        baseLayout.addComponent(viewContainer, 0, 1, 1, 1);
    }

    public User getUser(){
        return user;
    }

    private void restrictVisibility(){
        home.setVisible(true);
        requestFreeTime.setVisible(true);
        account.setVisible(true);
        login.setVisible(true);
        register.setVisible(true);
        User user = getUser();
        if(user != null){
            if(user.getType().equals(UserType.ADMIN)){
                menuBar.getItems().forEach(item -> item.setVisible(true));
                return;
            }
            boolean worker = user.getType().equals(UserType.EMPLOYEE);
            boolean boss = user.getType().equals(UserType.EMPLOYER);
            requestFreeTime.setVisible(worker || boss);
            approveFreeTime.setVisible(boss);
            addWorker.setVisible(boss);
            modifyWorker.setVisible(boss);
            logout.setVisible(true);
            admin.setVisible(false);
        } else {
            approveFreeTime.setVisible(false);
            addWorker.setVisible(false);
            modifyWorker.setVisible(false);
            logout.setVisible(false);
            admin.setVisible(false);
        }
    }
}
