package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.projekteszkozok.FreeTime.entity.Holiday;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.service.HolidayService;
import org.springframework.context.annotation.Scope;

import java.time.LocalDate;
import java.util.Locale;

@Scope("vaadin-ui")
@SpringView(name = RequestView.VIEW_NAME)
public class RequestView extends FormLayout implements View {

    public static final String VIEW_NAME = "request";

    private final HolidayService holidayService;

    private Binder<Holiday> binder;
    private User worker;

    public RequestView(HolidayService holidayService){
        this.holidayService = holidayService;
        initUser();
        initForm();
        initBinder();
    }

    /**
     * get logined user
     */
    private void initUser(){
        worker = ((AdminUI) UI.getCurrent()).getUser();
    }

    private void initForm(){
        setMargin(true);
        setSizeFull();
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
    }

    private void initBinder(){
        binder = new Binder<>();

        CheckBox paid = new CheckBox("Fizetett szabadságot szeretnél?");
        paid.setSizeUndefined();
        paid.setIcon(VaadinIcons.CHECK_CIRCLE);
        binder.forField(paid).bind(Holiday::isPaid, Holiday::setPaid);

        DateField startDate = new DateField("Szabadság kezdete:");
        startDate.setIcon(VaadinIcons.CALENDAR);
        startDate.setPlaceholder(LocalDate.now().toString());
        startDate.setRangeStart(LocalDate.now());
        startDate.setWidth(200, Unit.PIXELS);
        startDate.setLocale(new Locale("hu", "HU"));
        binder.forField(startDate).bind(Holiday::getStart, Holiday::setStart);

        DateField endDate = new DateField("Szabadság vége:");
        endDate.setIcon(VaadinIcons.CALENDAR);
        endDate.setPlaceholder(LocalDate.now().toString());
        endDate.setRangeStart(startDate.getValue());
        endDate.setWidth(200, Unit.PIXELS);
        endDate.setLocale(new Locale("hu", "HU"));
        binder.forField(endDate).bind(Holiday::getEnd, Holiday::setEnd);
        startDate.addValueChangeListener(event -> endDate.setRangeStart(startDate.getValue()));

        TextArea commentArea = new TextArea("Megjegyzés:");
        commentArea.setWidth(250, Unit.PIXELS);
        commentArea.setIcon(VaadinIcons.COMMENT);
        commentArea.setWordWrap(true);
        binder.forField(commentArea).bind(Holiday::getMessage, Holiday::setMessage);

        Button requestButton = new Button("Igénylés");
        requestButton.setIcon(VaadinIcons.PLUS);
        requestButton.setWidth(200, Unit.PIXELS);
        requestButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        requestButton.addClickListener(event -> {
            if(worker == null){
                Notification.show("Be kell jelentkezned az igényléshez!", Notification.Type.ERROR_MESSAGE);
                return;
            }
            Holiday toAdd = holidayService.add(binder.getBean());
            holidayService.addHoliday(worker, toAdd);
            Notification.show("Igény elküldve.", Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_RIGHT);
            getUI().getNavigator().navigateTo(HomeView.VIEW_NAME);
        });

        addComponents(startDate, endDate, commentArea, paid, requestButton);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("FreeTime - Igénylés");
        binder.setBean(new Holiday());
        if(worker != null){
            binder.getBean().setUser(worker);
        } else {
            Notification.show("Jelentkezz be!", Notification.Type.ERROR_MESSAGE);
        }
    }

}
