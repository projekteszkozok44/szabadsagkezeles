package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.User;

public interface EmailService {

    void sendRegistrationMessage(User user);

    void sendApprovedMessage(User user);

    void sendDeclinedMessage(User user);


}
