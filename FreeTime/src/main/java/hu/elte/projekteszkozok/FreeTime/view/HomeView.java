package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import org.springframework.context.annotation.Scope;


@Scope("vaadin-ui")
@SpringView(name = HomeView.VIEW_NAME)
public class HomeView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "home";


    public HomeView(){
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setMargin(true);
        showData();
    }

    private void showData() {
        Label greet = new Label("Üdv!\n" + "FREETIME AZ ÉLET.\n");
        greet.setSizeUndefined();
        addComponent(greet);
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("FreeTime - Főoldal");
    }

}
