package hu.elte.projekteszkozok.FreeTime.view;

import com.vaadin.data.provider.Query;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.projekteszkozok.FreeTime.entity.Holiday;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.service.EmailService;
import hu.elte.projekteszkozok.FreeTime.service.HolidayService;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Scope("vaadin-ui")
@SpringView(name = ApproveView.VIEW_NAME)
public class ApproveView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "approve";

    private final HolidayService holidayService;
    private final UserService userService;
    private final EmailService emailService;

    private final TextField nameFilter = new TextField();
    private User boss;
    private Grid<Holiday> requests;

    public ApproveView(HolidayService holidayService, UserService userService, EmailService emailService) {
        this.holidayService = holidayService;
        this.userService = userService;
        this.emailService = emailService;
        initUser();
        setHeightUndefined();
        initGrid();
        initFilter();
    }

    private void initGrid() {
        requests = new Grid<>();
        requests.setHeightMode(HeightMode.UNDEFINED);
        requests.setWidth(100, Unit.PERCENTAGE);
        requests.setBodyRowHeight(100);
        requests.addColumn(Holiday::getStart).setCaption("Szabadság kezdete");
        requests.addColumn(Holiday::getEnd).setCaption("Szabadság vége");
        requests.addColumn(Holiday::getUserName).setCaption("Dolgozó").setId("name");
        requests.addComponentColumn(requests -> {
            CheckBox paid = new CheckBox();
            paid.setReadOnly(true);
            if(requests.isPaid()){
                paid.setValue(true);
                return paid;
            } else {
                paid.setValue(false);
                return paid;
            }
        }).setCaption("Fizetett szabadság").setId("paid");
        requests.addColumn(Holiday::getMessage).setCaption("Megjegyzés");
        requests.addComponentColumn(this::buildApproveButton).setCaption("Jóváhagyás");
        requests.addComponentColumn(this::buildDeclineButton).setCaption("Elutasítás").setId("decline");
        requests.setStyleGenerator(item -> "v-align-center");
        addComponent(requests);
    }

    private void initFilter(){
        HeaderRow filterHeader = requests.appendHeaderRow();
        nameFilter.setPlaceholder("Szűrés vezetéknévre...");
        nameFilter.addValueChangeListener(e -> searchForFilters());
        nameFilter.setStyleName(ValoTheme.TEXTFIELD_TINY);
        nameFilter.setSizeUndefined();
        nameFilter.setValueChangeMode(ValueChangeMode.LAZY);
        Button clearFilterButton = new Button("Szűrés törlése");
        clearFilterButton.setStyleName(ValoTheme.BUTTON_TINY);
        clearFilterButton.addClickListener(e -> clearFilters());
        Button refreshButton = new Button("Frissítés");
        refreshButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
        refreshButton.addClickListener(e -> updateGrid());
        filterHeader.getCell("name").setComponent(nameFilter);
        filterHeader.getCell("paid").setComponent(clearFilterButton);
        filterHeader.getCell("decline").setComponent(refreshButton);
    }

    private void clearFilters(){
        nameFilter.clear();
    }

    private void searchForFilters(){
        List<Holiday> filtered = holidayService.listAll().stream().filter(x -> x.getUserName().toLowerCase().contains(nameFilter.getValue().toLowerCase())).collect(Collectors.toList());
        if(filtered.size() > 0){
            requests.setItems(filtered);
        } else{
            requests.setItems(Stream.empty());
            Notification.show("Nincs ilyen vezetéknév.", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void initUser(){
        boss = ((AdminUI) UI.getCurrent()).getUser();
    }

    private void updateGrid(){
        requests.getDataProvider().refreshAll();
        requests.setItems(holidayService.listAll());
    }

    private Button buildApproveButton(Holiday holiday){
        Button button = new Button(VaadinIcons.CHECK);
        button.addStyleName(ValoTheme.BUTTON_PRIMARY);
        button.addClickListener(e -> approveRequest(holiday));
        return button;
    }

    private void approveRequest(Holiday holiday) {
        User worker = userService.findByEmail(holiday.getUser().getEmail());
        worker.setOnHoliday(true);
        userService.updateUser(worker);
        holidayService.deleteHoliday(holiday);
        new Thread(() -> emailService.sendApprovedMessage(holiday.getUser())).start();
        updateGrid();
        Notification.show("Kérés jóváhagyva", Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_RIGHT);
    }

    private Button buildDeclineButton(Holiday holiday){
        Button button = new Button(VaadinIcons.CLOSE);
        button.addStyleName(ValoTheme.BUTTON_DANGER);
        button.addClickListener(e -> declineRequest(holiday));
        return button;
    }

    private void declineRequest(Holiday holiday) {
        User worker = userService.findByEmail(holiday.getUser().getEmail());
        worker.setOnHoliday(false);
        userService.updateUser(worker);
        holidayService.deleteHoliday(holiday);
        new Thread(() -> emailService.sendDeclinedMessage(holiday.getUser())).start();
        updateGrid();
        Notification.show("Kérés elutasítva", Notification.Type.TRAY_NOTIFICATION).setPosition(Position.TOP_RIGHT);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("FreeTime - Jóváhagyás");
        if(boss == null){
            Notification.show("Jelentkezz be!");
        }
        updateGrid();
        if(requests.getDataProvider().size(new Query<>()) == 0)
            Notification.show("Jelenleg nincs jóváhagyásra váró igény.", Notification.Type.ERROR_MESSAGE);
    }


}
