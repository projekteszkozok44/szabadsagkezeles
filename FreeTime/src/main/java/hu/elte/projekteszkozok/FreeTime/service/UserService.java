package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.User;

import java.util.List;

public interface UserService {

    User findByEmail(String email);

    User add(User user);

    void updateUser(User newUser);

    void deleteUser(User toDel);

    void deleteAllUsers();

    List<User> findAll();

    void updatePassword(User oldUser, User newUser);

}
