package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.repository.UserRepository;
import org.slf4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;

    private static final Logger LOGGER = getLogger(UserService.class);

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, EmailService emailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailService = emailService;
        LOGGER.info("UserService started...");
    }

    @Override
    public User findByEmail(String email) {
        LOGGER.info("Searching started for: " + email);
        return userRepository.findByEmail(email).orElseThrow(() ->
                new IllegalArgumentException("User with: " + email + " is not found."));
    }

    @Override
    public User add(User user) {
        Assert.notNull(user, "User can't be null!");
        if(!isRegisteredEmail(user.getEmail())){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setAuthority("ROLE_" + user.getType().toString());
            User toSave = userRepository.save(user);
            new Thread(() -> emailService.sendRegistrationMessage(toSave)).start();
            LOGGER.info(("New User added: " + toSave));
            return toSave;
        } else {
            LOGGER.info("This email is already registered: " + user.getEmail());
            throw new IllegalArgumentException("This email is already registered: " + user.getEmail());
        }
    }

    private boolean isRegisteredEmail(String email){
        List<User> all = userRepository.findAll();
        for(User u : all){
            if(u.getEmail().equals(email)){
                LOGGER.warn("Somebody already registered with: " + email);
                return true;
            }
        }
        return false;
    }

    @Override
    public void updateUser(User newUser) {
        User found = userRepository.findByEmail(newUser.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + newUser.getEmail() + " is not found."));
        BeanUtils.copyProperties(newUser, found, "email");
        BeanUtils.copyProperties(newUser, found);
        userRepository.save(found);
        LOGGER.info("User info has been updated: " + found);
    }

    @Override
    public void deleteUser(User toDel) {
        User found = userRepository.findByEmail(toDel.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + toDel.getEmail() + " is not found."));
        userRepository.delete(toDel);
        LOGGER.info("User was deleted: " + found);
    }

    @Override
    public void deleteAllUsers(){
        userRepository.deleteAll();
        LOGGER.info("All users have been deleted by ADMIN");
    }

    @Override
    public List<User> findAll() {
        LOGGER.info("All users have been listed.");
        return userRepository.findAll();
    }

    @Override
    public void updatePassword(User oldUser, User newUser) {
        User found = userRepository.findByEmail(oldUser.getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + oldUser.getEmail() + " is not found."));
        found.setPassword(passwordEncoder.encode(newUser.getPassword()));
        userRepository.save(found);
        LOGGER.info("User's password has been updated.");
    }


}
