package hu.elte.projekteszkozok.FreeTime.repository;

import hu.elte.projekteszkozok.FreeTime.entity.Holiday;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 *<H1>A HolidayRepository interfesz megvalositasa</H1>
 *
 * @since 2020.04.25.
 * @author Bettina
 */

@Repository
public interface HolidayRepository extends JpaRepository<Holiday, Long> {
    Optional<Holiday> findById(Long id);
    Optional<Holiday> findByUser(User user);

}
