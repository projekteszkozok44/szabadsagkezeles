package hu.elte.projekteszkozok.FreeTime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *<H1>FreeTimeApplication - a main fuggveny</H1>
 *
 * @since 2020.04.25.
 * @author Bettina
 */

@SpringBootApplication
public class FreeTimeApplication {

	/**
	 *A main függvény.
	 */
	public static void main(String[] args) {
		SpringApplication.run(FreeTimeApplication.class, args);
	}

}
