package hu.elte.projekteszkozok.FreeTime.entity;


import javax.persistence.*;
import java.time.LocalDate;

/**
 *<H1>Az entity osztalyban a Holiday ososztaly megvalositasa</H1>
 *
 * A szabadsag adatait taroljuk itt: ID, felhasznalo, fizetett-e, uzenet, napok.
 * @since 2020.04.25.
 * @author Bettina
 */

@Entity
public class Holiday {

    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    private User user;
    private String userName;
    private boolean isPaid;
    private String message;
    private LocalDate start;
    private LocalDate end;

     /**
     * Az ID gettere.
     *
     * @return A Holiday ID-je.
     */
    public Long getId() {
        return id;
    }
     /**
     * Az ID settere.
     *
     * A Holiday ID-jet valtoztatja meg.
     */
    public void setId(Long id) {
        this.id = id;
    }
	
    /**
     * Az User gettere.
     *
     * @return A Holidayhoz tartozo felhasznalo.
     */
    public User getUser() {
        return user;
    }

    /**
     * Az User settere.
     *
     * A Holiday felhasznalojat valtoztatja meg.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Az isPaid gettere.
     *
     * @return A Holidayhoz tartozo fizetettseg.
     */
    public boolean isPaid() {
        return isPaid;
    }

    /**
     * Az isPaid settere.
     *
     * A Holiday fizetettseget valtoztatja meg.
     */
    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    /**
     * A message gettere.
     *
     * @return A Holidayhoz tartozo uzenet.
     */
    public String getMessage() {
        return message;
    }

    /**
     * A message settere.
     *
     * A Holiday uzenetet valtoztatja meg.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }
}
