package hu.elte.projekteszkozok.FreeTime.security;


import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.repository.UserRepository;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository repository;
    private final UserService service;

    public MyUserDetailsService(UserRepository repository, UserService service) {
        this.repository = repository;
        this.service = service;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User found = repository.findByEmail(email).orElseThrow(() -> new IllegalArgumentException("User with: " + email + " is not found."));
        return new MyUserDetails(found);
    }


}
