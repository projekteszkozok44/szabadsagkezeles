package hu.elte.projekteszkozok.FreeTime.view;


import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.service.HolidayService;
import hu.elte.projekteszkozok.FreeTime.service.UserService;
import org.springframework.context.annotation.Scope;

@Scope("vaadin-ui")
@SpringView(name = AdminView.VIEW_NAME)
public class AdminView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "admin";

    private final UserService userService;
    private final HolidayService holidayService;

    private Grid<User> users;

    public AdminView(UserService userService, HolidayService holidayService) {
        this.userService = userService;
        this.holidayService = holidayService;
        setDefaultComponentAlignment(Alignment.TOP_CENTER);
        addButtons();
    }

    private void addButtons() {
        Button deleteUsersButton = new Button("Összes felhasználó törlése");
        Button deleteRequestsButton = new Button("Összes igénylés törlése");
        deleteRequestsButton.setStyleName(ValoTheme.BUTTON_DANGER);
        deleteRequestsButton.addClickListener(event -> {
            holidayService.deleteAllHolidays();
            Notification.show("Összes igénylés törölve").setPosition(Position.TOP_RIGHT);
        });
        deleteUsersButton.setStyleName(ValoTheme.BUTTON_DANGER);
        deleteUsersButton.addClickListener(event -> {
            userService.deleteAllUsers();
            Notification.show("Összes felhasználó törölve").setPosition(Position.TOP_RIGHT);
        });
        HorizontalLayout buttons = new HorizontalLayout(deleteUsersButton, deleteRequestsButton);
        addComponent(buttons);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent){
        getUI().getPage().setTitle("FreeTime - ADMIN");
    }
}
