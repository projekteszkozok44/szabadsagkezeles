package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.Holiday;
import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.repository.HolidayRepository;
import hu.elte.projekteszkozok.FreeTime.repository.UserRepository;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@Service
public class HolidayServiceImpl implements HolidayService {

    private final HolidayRepository holidayRepository;
    private final UserRepository userRepository;

    private static final Logger LOGGER = getLogger(UserService.class);

    public HolidayServiceImpl(HolidayRepository holidayRepository, UserRepository userRepository) {
        this.holidayRepository = holidayRepository;
        this.userRepository = userRepository;
        LOGGER.info("HolidayService started...");
    }

    @Override
    public Holiday add(Holiday holiday) {
        Assert.notNull(holiday, "Null holiday can't be saved.");
        Holiday toSave = holidayRepository.save(holiday);
        LOGGER.info("New Holiday added: " + toSave);
        return toSave;
    }

    @Override
    public void deleteHoliday(Holiday toDel) {
        Holiday found = holidayRepository.findById(toDel.getId()).orElseThrow(() ->
                new IllegalArgumentException("Holiday with id: " + toDel.getId() + " is not found."));
        User user = userRepository.findByEmail(found.getUser().getEmail()).orElseThrow(() ->
                new IllegalArgumentException("User with: " + found.getUser().getEmail() + " is not found."));
        user.setHoliday(null);
        userRepository.save(user);
        holidayRepository.delete(toDel);
        LOGGER.info("Holiday was deleted: " + found);
    }

    @Override
    public void deleteAllHolidays(){
        holidayRepository.deleteAll();
        LOGGER.info("All Holidays have been deleted by ADMIN");
    }


    @Override
    public void addHoliday(User user, Holiday holiday) {
        Assert.notNull(user, "Null User can't go to Holiday.");
        Assert.notNull(holiday, "Null Holiday can't be added.");
        Optional<User> foundUser = userRepository.findByEmail(user.getEmail());
        Optional<Holiday> foundHoliday = holidayRepository.findById(holiday.getId());
        if(foundHoliday.isPresent() && foundUser.isPresent()){
            User toSave = foundUser.get();
            Holiday toAdd = foundHoliday.get();
            toAdd.setUserName(toSave.getFirstName() + " " + toSave.getLastName());
            toSave.setHoliday(toAdd);
            toAdd.setUser(toSave);
            userRepository.save(toSave);
            holidayRepository.save(toAdd);
            LOGGER.info("New Holiday added to User: " + toAdd + " -- " + toSave);
        } else {
            LOGGER.info("User or Holiday was not found.");
        }
    }

    @Override
    public List<Holiday> listAll() {
        LOGGER.info("All holiday requests was listed.");
        return holidayRepository.findAll();
    }

}
