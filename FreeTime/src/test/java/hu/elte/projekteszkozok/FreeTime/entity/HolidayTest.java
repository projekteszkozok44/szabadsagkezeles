package hu.elte.projekteszkozok.FreeTime.entity;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class HolidayTest {

    @Test
    public void IdEquals(){
		Holiday tester = new Holiday();
        tester.setId(5L);
        Long expected = 5L;
		assertEquals(expected, tester.getId());
    }
	
	@Test
    public void MessageEquals(){
		Holiday tester = new Holiday();
        tester.setMessage("ok");
		assertEquals("ok", tester.getMessage());
    }
}
