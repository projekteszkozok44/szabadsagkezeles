package hu.elte.projekteszkozok.FreeTime.service;

import hu.elte.projekteszkozok.FreeTime.entity.User;
import hu.elte.projekteszkozok.FreeTime.repository.UserRepository;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    private User user;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setFirstName("Kovacs");
        user.setLastName("Jancsi");
        user.setEmail("kovacsjancsi@asd.hu");
        userRepository.save(user);
    }

//    @Test
//    public void findByEmail_shouldCallRepo(){
//        //GIVEN
//        when(userRepository.findByEmail("kovacsjancsi@asd.hu")).thenReturn(java.util.Optional.ofNullable(user));
//
//        //WHEN
//        User found = userService.findByEmail("kovacsjancsi@asd.hu");
//
//        //THEN
//        Assert.assertNotNull(found);
//        verify(userRepository, times(1)).findByEmail(anyString());
//        verifyNoMoreInteractions(userRepository);
//    }




}
