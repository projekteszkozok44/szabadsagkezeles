package hu.elte.projekteszkozok.FreeTime.repository;

import hu.elte.projekteszkozok.FreeTime.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void findByEmailShouldReturnUser(){
        //given
        User user = new User();
        user.setFirstName("Kovacs");
        user.setLastName("Janos");
        user.setEmail("kovacsjanos@asdf.hu");
        entityManager.persist(user);
        entityManager.flush();

        //when
        Optional<User> found = userRepository.findByEmail(user.getEmail());
        if(found.isPresent()){
            User present = found.get();

            //then
            assertThat(present.getEmail()).isEqualTo(user.getEmail());
        }
    }



}
