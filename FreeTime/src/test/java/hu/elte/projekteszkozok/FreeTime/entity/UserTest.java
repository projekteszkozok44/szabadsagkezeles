package hu.elte.projekteszkozok.FreeTime.entity;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class UserTest {

    @Test
    public void IdEquals(){
		User tester = new User();
        tester.setId(5L);
        Long expected = 5L;
		assertEquals(expected, tester.getId());
    }
	
	@Test
    public void FirstNameEquals(){
		User tester = new User();
        tester.setFirstName("ok");
		assertEquals("ok", tester.getFirstName());
    }
	
	@Test
    public void LastNameEquals(){
		User tester = new User();
        tester.setLastName("ok");
		assertEquals("ok", tester.getLastName());
    }
	
	@Test
    public void EmailEquals(){
		User tester = new User();
        tester.setEmail("ok");
		assertEquals("ok", tester.getEmail());
    }
	
	@Test
    public void PhoneEquals(){
		User tester = new User();
        tester.setPhone("ok");
		assertEquals("ok", tester.getPhone());
    }
	
	@Test
    public void PasswordEquals(){
		User tester = new User();
        tester.setPassword("ok");
		assertEquals("ok", tester.getPassword());
    }
	
	@Test
    public void OnHolidayEquals(){
		User tester = new User();
        tester.setOnHoliday(true);
        assertTrue(tester.isOnHoliday());
    }
	
	@Test
    public void MonthlyHoursEquals(){
		User tester = new User();
        tester.setMonthlyHours("5");
		assertEquals("5", tester.getMonthlyHours());
    }
	
	@Test
    public void DailyHoursEquals(){
		User tester = new User();
        tester.setDailyHours("5");
		assertEquals("5", tester.getDailyHours());
    }
}